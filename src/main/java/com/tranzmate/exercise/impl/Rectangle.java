package com.tranzmate.exercise.impl;

import com.tranzmate.exercise.IRectangle;
import com.tranzmate.exercise.exception.InvalidCoordinatesException;

import java.util.Objects;
import java.util.Properties;

public class Rectangle implements IRectangle {
    private static final String NAME = "NAME";

    private int left = 0;
    private int top = 0;
    private int right = 0;
    private int bottom = 0;
    private Properties properties = new Properties();

    public Rectangle() {
    }

    public Rectangle(int left, int top, int right, int bottom) {

        if (left > right ||
            bottom > top) {
            throw new InvalidCoordinatesException(left, top, right, bottom);
        }

        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public Rectangle(int left, int top, int right, int bottom, String name) {
        this(left, top, right, bottom);
        this.properties.setProperty(NAME, name);
    }

    public Rectangle(int left, int top, int right, int bottom, Properties properties) {
        this(left, top, right, bottom);
        this.properties = properties;
    }

    public int getLeft() {
        return left;
    }

    public int getTop() {
        return top;
    }

    public int getRight() {
        return right;
    }

    public int getBottom() {
        return bottom;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return left == rectangle.left &&
                top == rectangle.top &&
                right == rectangle.right &&
                bottom == rectangle.bottom;
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, top, right, bottom);
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":" + properties.getProperty(NAME) +
                '}';
    }
}
