package com.tranzmate.exercise.impl;

import com.tranzmate.exercise.IRectangle;
import com.tranzmate.exercise.IRectanglesStore;
import com.tranzmate.exercise.exception.InitializationException;
import com.tranzmate.exercise.exception.RectangleOutOfBoundsException;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TreeStore implements IRectanglesStore {
    private IRectangle bounds = null;

    private CoordinateProjection xProj;
    private CoordinateProjection yProj;

    @Override
    public void initialize(IRectangle bounds, Collection<IRectangle> rectangles) {
        if (bounds == null) {
            throw new InitializationException();
        }
        this.bounds = bounds;

        this.xProj = new CoordinateProjection(bounds.getLeft());
        this.yProj = new CoordinateProjection(bounds.getBottom());

        if (rectangles != null) {
            for (IRectangle r: rectangles) {
                if (r.getTop() > this.bounds.getTop() ||
                        r.getBottom() < this.bounds.getBottom() ||
                        r.getLeft() < this.bounds.getLeft() ||
                        r.getRight() > this.bounds.getRight()
                ) {
                    throw new RectangleOutOfBoundsException();
                }

                this.xProj.add(r.getLeft(), r.getRight(), r);
                this.yProj.add(r.getBottom(), r.getTop(), r);
            }
        }
    }

    @Override
    public IRectangle findRectangleAt(int x, int y) {
        List<IRectangle> listx = this.xProj.get(x);
        List<IRectangle> listy = this.yProj.get(y);

        if (listx.isEmpty() || listy.isEmpty()) {
            return null;
        }

        LinkedList<IRectangle> resultList = listx.stream()
                    .filter(listy::contains)
                    .collect(Collectors.toCollection(LinkedList::new));

        return resultList.isEmpty() ? null : resultList.getLast();
    }
}
