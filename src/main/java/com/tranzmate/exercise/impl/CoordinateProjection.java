package com.tranzmate.exercise.impl;

import com.tranzmate.exercise.IRectangle;

import java.util.LinkedList;
import java.util.TreeMap;

class CoordinateProjection {
    private TreeMap<Integer, LinkedList<IRectangle>> projection = new TreeMap<>();

    CoordinateProjection(int min_coordinate) {
        LinkedList<IRectangle> initialList = new LinkedList<>();
        initialList.add(null);
        this.projection.put(min_coordinate, initialList);
    }

    void add(int c1, int c2, IRectangle rectangle) {

        LinkedList<IRectangle> currentItem = this.getLayer(c1);
        currentItem.add(rectangle);

        LinkedList<IRectangle> lastItem = this.getLayer(c2);

        this.projection.subMap(c1, false, c2, false)
                .forEach(
                        (i, l) -> l.add(rectangle)
                );

        this.projection.put(c1, currentItem);
        this.projection.put(c2, lastItem);
    }

    private LinkedList<IRectangle> getLayer(int c) {
        Integer floorKey = this.projection.floorKey(c);
        LinkedList<IRectangle> layer = this.projection.floorEntry(c).getValue(); //underlying layer

        LinkedList<IRectangle> res;
        if (floorKey < c) {
            // copy of layer
            res = new LinkedList<>(layer);
        } else {
            res = layer;
        }
        return res;
    }

    LinkedList<IRectangle> get(int c) {
        return this.projection.floorEntry(c).getValue();
    }

}
