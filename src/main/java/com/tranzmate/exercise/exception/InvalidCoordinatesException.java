package com.tranzmate.exercise.exception;

public class InvalidCoordinatesException extends RuntimeException {
    private int left;
    private int top;
    private int right;
    private int bottom;

    public InvalidCoordinatesException(int left, int top, int right, int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }
}
