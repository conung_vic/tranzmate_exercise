package com.tranzmate.exercise.impl;

import com.tranzmate.exercise.IRectangle;
import com.tranzmate.exercise.IRectanglesStore;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class TreeStoreTest {

    private IRectanglesStore store;

    void initialize1() {
        IRectangle bounds = new Rectangle(0, 7, 13, 0, "Bounds");
        IRectangle r1 = new Rectangle(1, 6, 3, 4, "R1");
        IRectangle r2 = new Rectangle(4, 7, 5, 4, "R2");
        IRectangle r3 = new Rectangle(6, 4, 8, 1, "R3");
        IRectangle r4 = new Rectangle(11, 6, 12, 5, "R4");
        IRectangle r5 = new Rectangle(9, 2, 10, 1, "R5");
        IRectangle r6 = new Rectangle(2, 5, 7, 2, "R6");
        IRectangle r7 = new Rectangle(3, 3, 11, 0, "R7");
        IRectangle r8 = new Rectangle(4, 2, 5, 1, "R8");
        IRectangle r9 = new Rectangle(9, 7, 12, 4, "R9");

        this.store = new TreeStore();
        this.store.initialize(bounds, Arrays.asList(r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    void init2() {
        IRectangle bounds = new Rectangle(0, 15, 15, 0, "Bounds");
        IRectangle r1 = new Rectangle(3,12,12,3,"R1");
        IRectangle r2 = new Rectangle(1,5,5,2,"R2");
        IRectangle r3 = new Rectangle(2,15,5,3,"R3");
        IRectangle r4 = new Rectangle(13,2,14,1,"R4");
        IRectangle r5 = new Rectangle(10,11, 15,9,"R5");
        IRectangle r6 = new Rectangle(9,14,11,7,"R6");
        IRectangle r7 = new Rectangle(0,13,10,11,"R7");

        this.store = new TreeStore();
        this.store.initialize(bounds, Arrays.asList(r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    void testFind() {
        this.init2();
        IRectangle r = this.store.findRectangleAt(13, 5);
        assertNull(r);

        r = this.store.findRectangleAt(1, 14);
        assertNull(r);

        this.testRectangle("R3", 2, 3);
        this.testRectangle("R3", 4, 8);
        this.testRectangle("R7", 4, 12);
        this.testRectangle("R1", 8, 9);
        this.testRectangle("R5", 12, 10);
        this.testRectangle("R6", 10, 10);
        this.testRectangle("R5", 14, 10);
    }

    @Test
    void findRectangleAt() {
        this.initialize1();
        IRectangle r = this.store.findRectangleAt(7, 6);
        assertNull(r); // null

        this.testRectangle("R9", 10, 6);
        this.testRectangle("R7", 5, 2);
        this.testRectangle("R3", 7, 3);
        this.testRectangle("R1", 2, 5);
        this.testRectangle("R6", 5, 4);
    }

    private void testRectangle(String expectedName, int x, int y) {
        IRectangle r = this.store.findRectangleAt(x, y);
        assertEqualRect(expectedName, r, x, y);
    }

    private void assertEqualRect(String expected, IRectangle r, int x, int y) {
        assertNotNull(r);
        System.out.println(String.format("Point (%d, %d). Expected: %s; Actual: %s", x, y, expected, r.toString()));
        String name = r.getProperties().getProperty("NAME");
        assertEquals(expected, name);
    }
}