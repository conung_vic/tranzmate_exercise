### Task: The Rectangles Store

The requirements of this exercise are for you to implement a class that
stores rectangles. A rectangle is defined by the following interface:  
```
public interface IRectangle {  
    int getLeft();  
    int getTop();  
    int getRight();  
    int getBottom();  
    Properties getProperties();  
}
```
During its initialization process, your store will receive a rectangle 
that represents its bounds (e.g. the area in which other rectangles can appear),
and a collection of rectangles.   

You are required to store these rectangles in an efficient manner 
(in terms of memory consumption), and to later return the topmost 
rectangle per specified x, y location (or null in case no rectangle exists
in the specified location) in the most efficient way
possible in terms of performance.

***Note**: Assume that the solution should support a large number of rectangles, 
and that the bounding rectangle can be extremely large, so a solution
containing a simple collection of rectangles isn't efficient enough
performance-wise, and a solution containing a map of each point to its
corresponding rectangle isn't efficient enough memory-wise.*

The interface that you should implement is defined as:
```
public interface IRectanglesStore {
    void initialize(IRectangle bounds, Collection<IRectangle> rectangles);
    IRectangle findRectangleAt(int x, int y);
}
```

## Solution:

#### Initialization
I suggest to store rectangles as a projections of rectangles coordinates to X and Y axis.  
Each axis is represent as a `TreeMap<Integer, LinkedList<IRectangle>>`  
Key value - value of the coordinate on the axis  
List - List of the rectangles, started from this coordinate (see attached images), 
it looks like layers list from this coordinate and right (top).  

You can see attached excel file for detailed example.

#### Search rectangle by dot

Given point P(xp, yp).  
1. Get node from axis projection (tree) with maximum key_value equal or less of xp (yp).    
   Search in the tree - O(log n)  
2. Get lists from these nodes (ListX and ListY). O(1)  
3. If one of them is empty - P doesn't belong any rectangle. O(1)  
4. Build CrossList - contains common elements from ListX, ListY.  
   I think complexity of this operation about O(n1^2),   
   where n1 - average count of overlapping rectangles (average count of 'layers').   
5. if  CrossList is empty -  P doesn't belong any rectangle. O(1)  
6. Get LAST rectangle from the CrossList as Result. O(1) (from realization of LinkedList)  
